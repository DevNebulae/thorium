module Thorium.Aff.Util
	( awaitBody
	, awaitLoad
	, runThoriumAff
	, selectElement
	) where

import Control.Monad.Aff (Aff, makeAff, runAff)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Class (liftEff)
import Control.Monad.Eff.Exception (error, throwException)
import Control.Monad.Error.Class (throwError)
import Control.Monad.Except (runExcept)
import DOM (DOM)
import DOM.Event.EventTarget (addEventListener, eventListener)
import DOM.HTML (window)
import DOM.HTML.Event.EventTypes (load)
import DOM.HTML.Types (HTMLElement, htmlDocumentToParentNode, readHTMLElement, windowToEventTarget)
import DOM.HTML.Window (document)
import DOM.Node.ParentNode (QuerySelector(..), querySelector)
import Data.Either (either)
import Data.Foreign (toForeign)
import Data.Maybe (Maybe(..), maybe)
import Prelude (Unit, bind, const, discard, pure, unit, void, ($), (<<<), (<=<), (=<<), (>>=), (>>>))
import Thorium.Aff.Effects (ThoriumEffects)

-- | Wait for the body of the HTML document to load. Comparable to
-- | `$(document).on("load", () => ...)` when using jQuery.
awaitBody :: forall eff. Aff (dom :: DOM | eff) HTMLElement
awaitBody = do
	awaitLoad
	body <- selectElement (QuerySelector "body")
	maybe (throwError $ error "The body element could not be found in the HTML document") pure body

-- | Wait for the window of the HTML document to load. Comparable to
-- | `$(window).on("load", () => ...)` when using jQuery.
awaitLoad :: forall eff. Aff (dom :: DOM | eff) Unit
awaitLoad = makeAff \_ success -> liftEff $
	window
		>>= windowToEventTarget
		>>> addEventListener load (eventListener (\_ -> success unit)) false

runThoriumAff :: forall eff x. Aff (ThoriumEffects eff) x -> Eff (ThoriumEffects eff) Unit
runThoriumAff = void <<< runAff throwException (const (pure unit))

-- | Select an element from the DOM based on the specified `QuerySelector`. It
-- | will only select one element.
selectElement :: forall eff. QuerySelector -> Aff (dom :: DOM | eff) (Maybe HTMLElement)
selectElement query = do
	maybeElement <- liftEff $ ((querySelector query <<< htmlDocumentToParentNode <=< document) =<< window)

	pure case maybeElement of
		Nothing -> Nothing
		Just element -> either (const Nothing) Just $ runExcept $ readHTMLElement $ toForeign element
