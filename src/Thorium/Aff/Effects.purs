module Thorium.Aff.Effects where

import Control.Monad.Eff.Exception (EXCEPTION)
import DOM (DOM)

-- | Effects which regularly happen within a Thorium application.
type ThoriumEffects eff =
	( dom :: DOM
	, exception :: EXCEPTION
	| eff
	)
