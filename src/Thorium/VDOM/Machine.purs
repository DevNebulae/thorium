module Thorium.VDOM.Machine
	( Machine
	, Step(..)
	, extract
	, halt
	, step
	) where

import Prelude (Unit)

type Machine m a b = a -> m (Step m a b)

data Step m a b = Step b (Machine m a b) (m Unit)

extract :: forall m a b. Step m a b -> b
extract (Step x _ _) = x

halt :: forall m a b. Step m a b -> m Unit
halt (Step _ _ h) = h

step :: forall m a b. Step m a b -> a -> m (Step m a b)
step (Step _ m _) = m
