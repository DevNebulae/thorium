module Thorium.VDOM.Types
	( Elements
	, ElementName(..)
	, HtmlElement(..)
	, Namespace(..)
	, VDom(..)
	) where

import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe)
import Data.Newtype (class Newtype)
import Prelude (class Eq, class Functor, class Ord)

type Elements props = Array (VDom props)

newtype ElementName = ElementName String

derive newtype instance eqElementName :: Eq ElementName
derive instance genericElementName :: Generic ElementName _
derive instance newtypeElementName :: Newtype ElementName _
derive newtype instance ordElementName :: Ord ElementName

data HtmlElement props = HtmlElement (Maybe Namespace) ElementName props

derive instance eqHtmlElement :: Eq props => Eq (HtmlElement props)
derive instance ordHtmlElement :: Ord props => Ord (HtmlElement props)
derive instance genericHtmlElement :: Generic props rep => Generic (HtmlElement props) _

instance functorHtmlElement :: Functor HtmlElement where
	map f (HtmlElement namespace name props) = HtmlElement namespace name (f props)

-- | Defines the namespace of both an HTML element as well as a property of an
-- | HTML element.
newtype Namespace = Namespace String

derive newtype instance eqNamespace :: Eq Namespace
derive instance genericNamespace :: Generic Namespace _
derive instance newtypeNamespace :: Newtype Namespace _
derive newtype instance ordNamespace :: Ord Namespace

data VDom props
	= Element (HtmlElement props) (Array (VDom props))
	| Comment String
	| Text String
