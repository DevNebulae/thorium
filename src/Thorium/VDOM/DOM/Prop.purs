module Thorium.VDOM.DOM.Prop
	( Prop(..)
	, Props
	, PropValue
	, propFromBoolean
	, propFromInt
	, propFromNumber
	, propFromString
	) where

import Data.Maybe (Maybe)
import Thorium.VDOM.Types (Namespace)
import Unsafe.Coerce (unsafeCoerce)

type Props = Array Prop

propFromBoolean :: Boolean -> PropValue
propFromBoolean = unsafeCoerce

propFromInt :: Int -> PropValue
propFromInt = unsafeCoerce

propFromNumber :: Number -> PropValue
propFromNumber = unsafeCoerce

propFromString :: String -> PropValue
propFromString = unsafeCoerce

-- | A `Prop` can either consist of an `Attribute` or a `Property`. Where an
-- | `Attribute` is defined as a tag which can be set on an element in HTML, but
-- | it has no accessor in Javascript. `Property` contains the accessor key to
-- | the Javascript value and the `PropValue` is the actual value of the
-- | property. Note that `class` can both be an `Attribute` and a `Property`.
data Prop
	= Attribute (Maybe Namespace) String String
	| Property String PropValue

-- | An opaque type which allows only of range of values to be qualified as a
-- | `PropValue`. The `PropValue` is mostly used in other modules to declare
-- | certain types as renderable.
foreign import data PropValue :: Type
