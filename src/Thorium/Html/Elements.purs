module Thorium.Html.Elements
	( Leaf
	, Node
	, element
	, elementNS
	, a
	, abbr
	, area
	, article
	, aside
	, audio
	, b
	, base
	, bdi
	, bdo
	, blockquote
	, br
	, button
	, canvas
	, caption
	, cite
	, code
	, col
	, colgroup
	, command
	, datalist
	, dd
	, del
	, details
	, dfn
	, dialog
	, div
	, dl
	, dt
	, em
	, embed
	, fieldset
	, figcaption
	, figure
	, footer
	, form
	, h1
	, h2
	, h3
	, h4
	, h5
	, h6
	, header
	, hr
	, i
	, iframe
	, img
	, input
	, ins
	, kbd
	, label
	, legend
	, li
	, link
	, main
	, map
	, mark
	, menu
	, menuitem
	, meta
	, meter
	, nav
	, noscript
	, object
	, ol
	, optgroup
	, option
	, output
	, p
	, param
	, pre
	, progress
	, q
	, rp
	, rt
	, ruby
	, samp
	, script
	, section
	, select
	, small
	, source
	, span
	, strong
	, style
	, sub
	, summary
	, sup
	, table
	, tbody
	, td
	, textarea
	, tfoot
	, th
	, thead
	, time
	, title
	, tr
	, track
	, u
	, ul
	, var
	, video
	, wbr
	) where

import DOM.HTML.Indexed as Indexed
import Data.Maybe (Maybe(..))
import Prelude (pure, (#), (>>>))
import Thorium.Html.Core (Html)
import Thorium.Html.Core (element) as Core
import Thorium.Html.Properties (IProp)
import Thorium.VDOM.DOM.Prop (Prop)
import Thorium.VDOM.Types (ElementName(..), Namespace)
import Unsafe.Coerce (unsafeCoerce)

-- | An HTML element that admits children.
type Node r
   = Array (IProp r)
  -> Array Html
  -> Html

-- | An HTML element that does not admit children.
type Leaf r
   = Array (IProp r)
  -> Html

element :: forall r. ElementName -> Array (IProp r) -> Array Html -> Html
element =
	Core.element Nothing #
		( unsafeCoerce
      :: (ElementName -> Array Prop -> Array Html -> Html)
			-> ElementName
			-> Array (IProp r)
      -> Array Html
      -> Html
		)

elementNS :: forall r. Namespace -> ElementName -> Array (IProp r) -> Array Html -> Html
elementNS =
  pure >>> Core.element >>>
    ( unsafeCoerce
      :: (ElementName -> Array Prop -> Array Html -> Html)
      -> ElementName
      -> Array (IProp r)
      -> Array Html
      -> Html
		)

a :: Node Indexed.HTMLa
a = element (ElementName "a")

abbr :: Node Indexed.HTMLabbr
abbr = element (ElementName "abbr")

area :: Leaf Indexed.HTMLarea
area properties = element (ElementName "area") properties []

article :: Node Indexed.HTMLarticle
article = element (ElementName "article")

aside :: Node Indexed.HTMLaside
aside = element (ElementName "aside")

audio :: Node Indexed.HTMLaudio
audio = element (ElementName "audio")

b :: Node Indexed.HTMLb
b = element (ElementName "b")

base :: Leaf Indexed.HTMLbase
base properties = element (ElementName "base") properties []

bdi :: Node Indexed.HTMLbdi
bdi = element (ElementName "bdi")

bdo :: Node Indexed.HTMLbdo
bdo = element (ElementName "bdo")

blockquote :: Node Indexed.HTMLblockquote
blockquote = element (ElementName "blockquote")

br :: Leaf Indexed.HTMLbr
br properties = element (ElementName "br") properties []

button :: Node Indexed.HTMLbutton
button = element (ElementName "button")

canvas :: Leaf Indexed.HTMLcanvas
canvas properties = element (ElementName "canvas") properties []

caption :: Node Indexed.HTMLcaption
caption = element (ElementName "caption")

cite :: Node Indexed.HTMLcite
cite = element (ElementName "cite")

code :: Node Indexed.HTMLcode
code = element (ElementName "code")

col :: Leaf Indexed.HTMLcol
col properties = element (ElementName "col") properties []

colgroup :: Node Indexed.HTMLcolgroup
colgroup = element (ElementName "colgroup")

command :: Leaf Indexed.HTMLcommand
command properties = element (ElementName "command") properties []

datalist :: Node Indexed.HTMLdatalist
datalist = element (ElementName "datalist")

dd :: Node Indexed.HTMLdd
dd = element (ElementName "dd")

del :: Node Indexed.HTMLdel
del = element (ElementName "del")

details :: Node Indexed.HTMLdetails
details = element (ElementName "details")

dfn :: Node Indexed.HTMLdfn
dfn = element (ElementName "dfn")

dialog :: Node Indexed.HTMLdialog
dialog = element (ElementName "dialog")

div :: Node Indexed.HTMLdiv
div = element (ElementName "div")

dl :: Node Indexed.HTMLdl
dl = element (ElementName "dl")

dt :: Node Indexed.HTMLdt
dt = element (ElementName "dt")

em :: Node Indexed.HTMLem
em = element (ElementName "em")

embed :: Node Indexed.HTMLembed
embed = element (ElementName "embed")

fieldset :: Node Indexed.HTMLfieldset
fieldset = element (ElementName "fieldset")

figcaption :: Node Indexed.HTMLfigcaption
figcaption = element (ElementName "figcaption")

figure :: Node Indexed.HTMLfigure
figure = element (ElementName "figure")

footer :: Node Indexed.HTMLfooter
footer = element (ElementName "footer")

form :: Node Indexed.HTMLform
form = element (ElementName "form")

h1 :: Node Indexed.HTMLh1
h1 = element (ElementName "h1")

h2 :: Node Indexed.HTMLh2
h2 = element (ElementName "h2")

h3 :: Node Indexed.HTMLh3
h3 = element (ElementName "h3")

h4 :: Node Indexed.HTMLh4
h4 = element (ElementName "h4")

h5 :: Node Indexed.HTMLh5
h5 = element (ElementName "h5")

h6 :: Node Indexed.HTMLh6
h6 = element (ElementName "h6")

header :: Node Indexed.HTMLheader
header = element (ElementName "header")

hr :: Leaf Indexed.HTMLhr
hr properties = element (ElementName "hr") properties []

i :: Node Indexed.HTMLi
i = element (ElementName "i")

iframe :: Leaf Indexed.HTMLiframe
iframe properties = element (ElementName "iframe") properties []

img :: Leaf Indexed.HTMLimg
img properties = element (ElementName "img") properties []

input :: Leaf Indexed.HTMLinput
input properties = element (ElementName "input") properties []

ins :: Node Indexed.HTMLins
ins = element (ElementName "ins")

kbd :: Node Indexed.HTMLkbd
kbd = element (ElementName "kbd")

label :: Node Indexed.HTMLlabel
label = element (ElementName "label")

legend :: Node Indexed.HTMLlegend
legend = element (ElementName "legend")

li :: Node Indexed.HTMLli
li = element (ElementName "li")

link :: Leaf Indexed.HTMLlink
link properties = element (ElementName "link") properties []

main :: Node Indexed.HTMLmain
main = element (ElementName "main")

map :: Node Indexed.HTMLmap
map = element (ElementName "map")

mark :: Node Indexed.HTMLmark
mark = element (ElementName "mark")

menu :: Node Indexed.HTMLmenu
menu = element (ElementName "menu")

menuitem :: Node Indexed.HTMLmenuitem
menuitem = element (ElementName "menuitem")

meta :: Leaf Indexed.HTMLmeta
meta properties = element (ElementName "meta") properties []

meter :: Node Indexed.HTMLmeter
meter = element (ElementName "meter")

nav :: Node Indexed.HTMLnav
nav = element (ElementName "nav")

noscript :: Node Indexed.HTMLnoscript
noscript = element (ElementName "noscript")

object :: Node Indexed.HTMLobject
object = element (ElementName "object")

ol :: Node Indexed.HTMLol
ol = element (ElementName "ol")

optgroup :: Node Indexed.HTMLoptgroup
optgroup = element (ElementName "optgroup")

option :: Node Indexed.HTMLoption
option = element (ElementName "option")

output :: Node Indexed.HTMLoutput
output = element (ElementName "output")

p :: Node Indexed.HTMLp
p = element (ElementName "p")

param :: Leaf Indexed.HTMLparam
param properties = element (ElementName "param") properties []

pre :: Node Indexed.HTMLpre
pre = element (ElementName "pre")

progress :: Node Indexed.HTMLprogress
progress = element (ElementName "progress")

q :: Node Indexed.HTMLq
q = element (ElementName "q")

rp :: Node Indexed.HTMLrp
rp = element (ElementName "rp")

rt :: Node Indexed.HTMLrt
rt = element (ElementName "rt")

ruby :: Node Indexed.HTMLruby
ruby = element (ElementName "ruby")

samp :: Node Indexed.HTMLsamp
samp = element (ElementName "samp")

script :: Node Indexed.HTMLscript
script = element (ElementName "script")

section :: Node Indexed.HTMLsection
section = element (ElementName "section")

select :: Node Indexed.HTMLselect
select = element (ElementName "select")

small :: Node Indexed.HTMLsmall
small = element (ElementName "small")

source :: Leaf Indexed.HTMLsource
source properties = element (ElementName "source") properties []

span :: Node Indexed.HTMLspan
span = element (ElementName "span")

strong :: Node Indexed.HTMLspan
strong = element (ElementName "strong")

style :: Node Indexed.HTMLstyle
style = element (ElementName "style")

sub :: Node Indexed.HTMLsub
sub = element (ElementName "sub")

summary :: Node Indexed.HTMLsummary
summary = element (ElementName "summary")

sup :: Node Indexed.HTMLsup
sup = element (ElementName "sup")

table :: Node Indexed.HTMLtable
table = element (ElementName "table")

tbody :: Node Indexed.HTMLtbody
tbody = element (ElementName "tbody")

td :: Node Indexed.HTMLtd
td = element (ElementName "td")

textarea :: Leaf Indexed.HTMLtextarea
textarea properties = element (ElementName "textarea") properties []

tfoot :: Node Indexed.HTMLtfoot
tfoot = element (ElementName "tfoot")

th :: Node Indexed.HTMLth
th = element (ElementName "th")

thead :: Node Indexed.HTMLthead
thead = element (ElementName "thead")

time :: Node Indexed.HTMLtime
time = element (ElementName "time")

title :: Node Indexed.HTMLtitle
title = element (ElementName "title")

tr :: Node Indexed.HTMLtr
tr = element (ElementName "tr")

track :: Leaf Indexed.HTMLtrack
track properties = element (ElementName "track") properties []

u :: Node Indexed.HTMLu
u = element (ElementName "u")

ul :: Node Indexed.HTMLul
ul = element (ElementName "ul")

var :: Node Indexed.HTMLvar
var = element (ElementName "var")

video :: Node Indexed.HTMLvideo
video = element (ElementName "video")

wbr :: Leaf Indexed.HTMLwbr
wbr properties = element (ElementName "wbr") properties []
