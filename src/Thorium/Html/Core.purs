module Thorium.Html.Core
	( class IsProp
	, AttributeName(..)
	, ClassName(..)
	, Html(..)
	, PropertyName(..)
	, attribute
	, comment
	, element
	, property
	, text
	, toPropValue
	) where

import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe)
import Data.Newtype (class Newtype, unwrap)
import Prelude (class Eq, class Ord, ($), (<<<))
import Thorium.VDOM.DOM.Prop (Prop(..), PropValue, Props, propFromBoolean, propFromInt, propFromNumber, propFromString)
import Thorium.VDOM.Types (ElementName, HtmlElement(..), Namespace, VDom(..))
import Unsafe.Coerce (unsafeCoerce)

-- | Smart constructor which creates an `Attribute` with the given namespace and
-- | the given name.
attribute :: Maybe Namespace -> AttributeName -> String -> Prop
attribute namespace name = Attribute namespace $ unwrap name

-- | Smart constructor which creates a `Comment` element with the given textual
-- | content.
comment :: String -> Html
comment = Html <<< Comment

-- | Smart constructor which creates an `Element` with an `HtmlElement` as its
-- | inner value. It unsafely coerces the `Namespace`, `ElementName,
-- | `Array Prop` and children to build an `HtmlElement`.
element :: Maybe Namespace -> ElementName -> Props -> Array Html -> Html
element namespace = coe (\name properties children -> Element (HtmlElement namespace name properties) children)
	where
	coe
		:: (ElementName -> Props -> Array (VDom Props) -> VDom Props)
		-> ElementName
		-> Props
		-> Array Html
		-> Html
	coe = unsafeCoerce

-- | Smart constructor which creates a `Property` with the given name and its
-- | associated value. The `IsProp` type class guarantees the value can be
-- | coerced to a `PropValue` type so it knows how to render the value.
property :: forall value. IsProp value => PropertyName value -> value -> Prop
property (PropertyName name) = Property name <<< toPropValue

-- | Smart constructor which creates a `Text` element with the given textual
-- | content.
text :: String -> Html
text = Html <<< Text

class IsProp a where
	toPropValue :: a -> PropValue

instance booleanIsProp :: IsProp Boolean where
	toPropValue = propFromBoolean

instance intIsProp :: IsProp Int where
	toPropValue = propFromInt

instance numberIsProp :: IsProp Number where
	toPropValue = propFromNumber

instance stringIsProp :: IsProp String where
	toPropValue = propFromString

newtype AttributeName = AttributeName String

derive newtype instance eqAttributeName :: Eq AttributeName
derive instance genericAttributeName :: Generic AttributeName _
derive instance newtypeAttributeName :: Newtype AttributeName _
derive newtype instance ordAttributeName :: Ord AttributeName

newtype ClassName = ClassName String

derive newtype instance eqClassName :: Eq ClassName
derive instance genericClassName :: Generic ClassName _
derive instance newtypeClassName :: Newtype ClassName _
derive newtype instance ordClassName :: Ord ClassName

-- | Type wrapper which wraps a virtual DOM in an `Html` constructor. It is more
-- | readable than writing `VDom` all over the place.
newtype Html = Html (VDom Props)

derive instance newtypeHtml :: Newtype Html _

newtype PropertyName value = PropertyName String

derive newtype instance eqPropertyName :: Eq value => Eq (PropertyName value)
derive instance genericPropertyName :: Generic value r => Generic (PropertyName value) _
derive instance newtypePropertyName :: Newtype value t => Newtype (PropertyName value) _
derive newtype instance ordPropertyName :: Ord value => Ord (PropertyName value)
