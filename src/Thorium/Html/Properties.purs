module Thorium.Html.Properties
	( IProp(..)
	, attribute
	, attributeNS
	, class'
	, classes
	, id
	, src
	) where

import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, unwrap)
import Data.String (joinWith)
import Prelude (map, pure, (#), (<<<), (>>>))
import Thorium.Html.Core (class IsProp, AttributeName, ClassName, PropertyName(..))
import Thorium.Html.Core (attribute, property) as Core
import Thorium.VDOM.DOM.Prop (Prop)
import Thorium.VDOM.Types (Namespace)
import Unsafe.Coerce (unsafeCoerce)

attribute :: forall r. AttributeName -> String -> IProp r
attribute = Core.attribute Nothing #
	unsafeCoerce
		:: (AttributeName -> String -> Prop)
		-> AttributeName
		-> String
		-> IProp r

attributeNS :: Namespace -> AttributeName -> String -> Prop
attributeNS =
	pure >>> Core.attribute >>>
		( unsafeCoerce
			:: (AttributeName -> String -> Prop)
			-> AttributeName
			-> String
			-> Prop
		)

property :: forall value r. IsProp value => PropertyName value -> value -> IProp r
property =
	( unsafeCoerce
		:: (PropertyName value -> value -> Prop)
		-> PropertyName value
		-> value
		-> IProp r
	) Core.property

class' :: forall r. ClassName -> IProp (class :: String | r)
class' = property (PropertyName "className") <<< unwrap

classes :: forall r. Array ClassName -> IProp (class :: String | r)
classes = property (PropertyName "className") <<< joinWith " " <<< map unwrap

id :: forall r. String -> IProp (id :: String | r)
id = property (PropertyName "id")

src :: forall r. String -> IProp (src :: String | r)
src = property (PropertyName "src")

-- | Wrapper type for `Prop` which gives the control of defining which `Props`
-- | are allowed to be set on a specific element.
newtype IProp (r :: # Type) = IProp Prop

derive instance newtypeIProp :: Newtype (IProp r) _
